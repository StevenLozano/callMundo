<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Call Center AlMundo</title>
<meta http-equiv="refresh" content="2" />
</head>
<body>
	<form:form action="paginaPrincipal" method="post">
		<table border="0px" width="100%">
			<tr>
				<th width="50px">
					<img src="/mundo/resources/images/logo.png" width="200px" height="55px"/>
				</th>
				<th>
					<font color="#ea6422" face="Comic Sans MS">
						<marquee direction="right" behavior="alternate">			
							<h1>BIENVENIDOS AL CALL CENTER</h1>
						</marquee>
					</font>
				</th>
			</tr>
			<tr>
				<th colspan="2" align="left">
					<font size="5px">Agregar Llamada</font>
					<input type = "image" src="/mundo/resources/images/call.png" width="30px" height="30px" style="margin-bottom: -8px;" name = "agregarLlamada"/>
				</th>
			</tr>
		</table>
		<table bgcolor="#179ea4" style="border:2px solid #000000">
			<tr>
				<th>
					<div style="background-color:#179ea4;font-weight: bold;" align="center">
						<br/>
						<label>
							DATOS LLAMADAS
						</label>
						<br/><br/>
						<table id ="datosLlamadas" style="border:2px solid #FFFFFF">
							<tr>
								<th align="center" style="border:1px solid #FFFFFF">TOTAL LLAMADAS</th>
								<th align="center" style="border:1px solid #FFFFFF">LLAMADAS EN ESPERA</th>
								<th align="center" style="border:1px solid #FFFFFF">LLAMADAS FINALIZADAS</th>
							</tr>
							<tr>
								<th align="center">${totalLlamadas}</th>
								<th align="center">${llamadasEnEspera}</th>
								<th align="center">${llamadasFinalizadas}</th>
							</tr>
						</table>
						<br/>
						<label>
							DATOS LLAMADAS EN ESPERA
						</label>
						<br/><br/>
						<table id ="colaLlamadasEspera" style="border:2px solid #FFFFFF">
							<tr>
								<th align="center" style="border:1px solid #FFFFFF">ID</th>
								<th align="center" style="border:1px solid #FFFFFF">ID CLIENTE</th>
								<th align="center" style="border:1px solid #FFFFFF">FECHA ENTRADA</th>
								<th align="center" style="border:1px solid #FFFFFF">TIEMPO LLAMADA</th>
							</tr>
							<c:forEach items="${colaLlamadas}" var="colaLlamadas">
								<tr>
									<th align="center">${colaLlamadas.id}</th>
									<th align="center">${colaLlamadas.idCliente}</th>
									<th align="center">${colaLlamadas.fechaEntrada}</th>
									<th align="center">${colaLlamadas.tiempo}</th>
								</tr>
							</c:forEach>
						</table>
						<br/>
					</div>
				<th>
				<th>
					<div style="background-color:#179ea4;font-weight: bold;" align="center">
						<br/>
						<label>
							LISTA EMPLEADOS
						</label>
						<br/><br/>
						<table id ="listaEmpleados"  style="border:2px solid #FFFFFF">
							<tr style="border:1px solid">
								<th align="center" style="border:1px solid">ID</th>
								<th align="center" style="border:1px solid">NOMBRES</th>
								<th align="center" style="border:1px solid">DOCUMENTO</th>
								<th align="center" style="border:1px solid">TIPO EMPLEADO</th>
								<th align="center" style="border:1px solid">LLAMADAS ATENDIDAS</th>
								<th align="center" style="border:1px solid">LIBRE</th>
							</tr>
							<c:forEach items="${listaEmpleados}" var="listEmpleados">
								<tr>
									<th>${listEmpleados.id}</th>
									<th>${listEmpleados.nombres}</th>
									<th>${listEmpleados.documento}</th>
									<th>${listEmpleados.tipoEmpleado.descripcion}</th>
									<th>${listEmpleados.llamadasAtendidas}</th>
									<th>
									<c:if test="${listEmpleados.estaLibre}" >SI</c:if>
									<c:if test="${listEmpleados.estaLibre == false}" >NO</c:if>
									</th>
								</tr>
							</c:forEach>
						</table>
						<br/>
						<input type = "submit" name = "agregarEmpleado" value="Agregar"/>
						<br/>
					</div>
				</th>
			</tr>
		</table>
	</form:form>
</body>
</html>