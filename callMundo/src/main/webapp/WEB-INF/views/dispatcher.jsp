<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Call Center AlMundo</title>
</head>
<body>
	<table border="0px" width="100%">
		<tr>
			<th width="50px">
				<img src="/mundo/resources/images/logo.png" width="200px" height="55px"/>
			</th>
			<th>
				<font color="#ea6422" face="Comic Sans MS">
					<marquee direction="right" behavior="alternate">			
						<h1>BIENVENIDOS AL CALL CENTER</h1>
					</marquee>
				</font>
			</th>
		</tr>
		<tr>
			<th colspan="2" align="left">
				<font size="5px">Para simular la entrada de una llamada presione:</font>
				<a href="/mundo"> 
					<img src="/mundo/resources/images/call.png" width="30px" height="30px" style="margin-bottom: -8px;"/>
				</a>		
			</th>
		</tr>
	</table>
	<div style="background-color:#179ea4;font-weight: bold;" align="center">
		<label> LISTA EMPLEADOS </label>
		<c:forEach items="${listaEmpleados}" var="listEmpleados">
			<li>  </li>
		</c:forEach>
		<table id ="listaEmpleados" bgcolor="#179ea4">
			
		</table>
	</div>
</body>
</html>