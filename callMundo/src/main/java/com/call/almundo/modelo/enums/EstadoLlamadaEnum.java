package com.call.almundo.modelo.enums;

/**
 * Enum para referenciar los estados de las llamadas del call center
 *  
 * @author Steven A. Lozano O.
 *
 */
public enum EstadoLlamadaEnum {
	ENTRANTE	( 1, "ENTRANTE"   ),
	EN_ESPERA	( 2, "EN ESPERA"  ),
	EN_PROCESO	( 3, "EN PROCESO"  ),
	FINALIZADA	( 4, "FINALIZADA" );
	
	private Integer pasos;
	private String  descripcion;
	
	EstadoLlamadaEnum ( Integer pasos, String descripcion ){
		this.pasos 			= pasos;
		this.descripcion 	= descripcion;
	}

	public Integer getPasos() {
		return pasos;
	}

	public void setPasos(Integer pasos) {
		this.pasos = pasos;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
