package com.call.almundo.modelo.enums;

/**
 * Enum para referenciar los tipos de empleados del call center
 * 
 * @author Steven A. Lozano O.
 *
 */
public enum TipoEmpleadoEnum {
	OPERADOR	( 1, "OPERADOR"   ),
	SUPERVISOR	( 2, "SUPERVISOR" ),
	DIRECTOR	( 3, "DIRECTOR"   );
	
	private Integer prioridad;
	private String 	descripcion;
	
	TipoEmpleadoEnum ( Integer prioridad, String descripcion ) {
		this.prioridad 		= prioridad;
		this.descripcion 	= descripcion;
	}

	public Integer getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public static TipoEmpleadoEnum obtenerPorPrioridad ( Integer prioridad ) {
		for (TipoEmpleadoEnum tmp: TipoEmpleadoEnum.values() ) {
			if (tmp.prioridad == prioridad) {
				return tmp;
			}
		}
		return null;
	}
}
