package com.call.mundo.modelo;

import com.call.almundo.modelo.enums.TipoEmpleadoEnum;

/**
 * Referencia la informacion general y el cargo del empleador
 * @author Steven A. Lozano O.
 *
 */
public class Empleado {
	private Integer 			id;
	private String 				nombres;
	private	Integer				documento;
	private TipoEmpleadoEnum	tipoEmpleado;
	private Integer				llamadasAtendidas;
	private boolean				estaLibre;
	
	public Empleado ( Integer id, String nombres, Integer documento, TipoEmpleadoEnum tipoEmpleado ){
		this.id 				= id;
		this.nombres 			= nombres;
		this.documento 			= documento;
		this.tipoEmpleado 		= tipoEmpleado;
		this.llamadasAtendidas 	= 0;
		this.estaLibre			= true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public Integer getDocumento() {
		return documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public TipoEmpleadoEnum getTipoEmpleado() {
		return tipoEmpleado;
	}

	public void setTipoEmpleado(TipoEmpleadoEnum tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}

	public Integer getLlamadasAtendidas() {
		return llamadasAtendidas;
	}

	public void setLlamadasAtendidas(Integer llamadasAtendidas) {
		this.llamadasAtendidas = llamadasAtendidas;
	}

	public boolean isEstaLibre() {
		return estaLibre;
	}

	public void setEstaLibre(boolean estaLibre) {
		this.estaLibre = estaLibre;
	}
}
