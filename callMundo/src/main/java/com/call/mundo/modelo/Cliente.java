package com.call.mundo.modelo;
/**
 * De momento solo se tienen los datos basicos del cliente, pueden tomarse más datos dependiendo del
 * crecimiento del software
 * @author Steven A. Lozano O.
 *
 */
public class Cliente {
	private Integer id;
	private String	nombres;
	private Integer documento;
	
	public Cliente ( Integer id, String nombres, Integer documento ){
		this.id 		= id;
		this.nombres 	= nombres;
		this.documento	= documento;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public Integer getDocumento() {
		return documento;
	}
	
	public void setDocumento(Integer documento) {
		this.documento = documento;
	}
}
