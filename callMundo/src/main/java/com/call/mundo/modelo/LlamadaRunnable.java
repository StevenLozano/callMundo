package com.call.mundo.modelo;

import java.util.Date;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.call.mundo.controlador.DispatcherController;

public class LlamadaRunnable implements Runnable{
	
	private Llamada llamada;
	private static final Logger logger = LoggerFactory.getLogger(LlamadaRunnable.class);
	
	public LlamadaRunnable ( Llamada llamada ) {
		this.llamada 		= llamada;
	}
	
	@Override
	public void run () {
		
		logger.info("Log 1: " + llamada.getId() + " - " + llamada.getEstadoLlamada() + " tiempo: " + new Date() );
		
		boolean flag = false;
		
		do {
			if ( flag )
				esperar ( 1 );
			llamada = DispatcherController.dispatchCall(llamada);
			flag = true;
		} while ( Objects.isNull( llamada.getIdEmpleadoAtendio() ) );
		
		logger.info("Log 2: " + llamada.getId() + " - " + llamada.getEstadoLlamada() + " tiempo: " + new Date() );
		
		esperar (llamada.getTiempo()*10);
		
		DispatcherController.terminaLlamada (llamada);
		
		logger.info("Log 3: " + llamada.getId() + " - " + llamada.getEstadoLlamada() + " tiempo: " + new Date() );
	}
	
	private void esperar ( int segundos ) {
		try {
			Thread.sleep ( segundos * 100 );
		} catch ( InterruptedException ex ) {
			Thread.currentThread().interrupt();
		}
	}

}
