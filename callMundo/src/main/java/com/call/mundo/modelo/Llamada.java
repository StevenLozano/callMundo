package com.call.mundo.modelo;

import java.util.Date;

import com.call.almundo.modelo.enums.EstadoLlamadaEnum;

/**
 * Clase en la que se guarda la llamada y su estado con las referencias respectivas a quien pertenece
 * y quien atiende.
 * @author Steven A. Lozano O.
 *
 */
public class Llamada {
	
	private Integer 			id;
	private Integer				idCliente;
	private EstadoLlamadaEnum	estadoLlamada;
	private Date 				fechaEntrada;
	private Date 				fechaInicioAtencion;
	private Integer 			idEmpleadoAtendio;
	private Date 				fechaFinaliza;
	private Integer				tiempo;
	
	public Llamada ( Integer id, Integer idCliente, EstadoLlamadaEnum estadoLlamada, Date fechaEntrada, Integer tiempo ){
		this.id 			= id;
		this.idCliente 		= idCliente;
		this.estadoLlamada 	= estadoLlamada;
		this.fechaEntrada 	= fechaEntrada;
		this.tiempo			= tiempo;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getIdCliente() {
		return idCliente;
	}
	
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	
	public EstadoLlamadaEnum getEstadoLlamada() {
		return estadoLlamada;
	}
	
	public void setEstadoLlamada(EstadoLlamadaEnum estadoLlamada) {
		this.estadoLlamada = estadoLlamada;
	}
	
	public Date getFechaEntrada() {
		return fechaEntrada;
	}
	
	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}
	
	public Date getFechaInicioAtencion() {
		return fechaInicioAtencion;
	}
	
	public void setFechaInicioAtencion(Date fechaInicioAtencion) {
		this.fechaInicioAtencion = fechaInicioAtencion;
	}
	
	public Integer getIdEmpleadoAtendio() {
		return idEmpleadoAtendio;
	}
	
	public void setIdEmpleadoAtendio(Integer idEmpleadoAtendio) {
		this.idEmpleadoAtendio = idEmpleadoAtendio;
	}
	
	public Date getFechaFinaliza() {
		return fechaFinaliza;
	}
	
	public void setFechaFinaliza(Date fechaFinaliza) {
		this.fechaFinaliza = fechaFinaliza;
	}

	public Integer getTiempo() {
		return tiempo;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}
}
