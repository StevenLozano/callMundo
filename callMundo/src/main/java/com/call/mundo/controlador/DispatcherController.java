package com.call.mundo.controlador;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.call.almundo.modelo.enums.EstadoLlamadaEnum;
import com.call.almundo.modelo.enums.TipoEmpleadoEnum;
import com.call.mundo.modelo.Cliente;
import com.call.mundo.modelo.Empleado;
import com.call.mundo.modelo.Llamada;
import com.call.mundo.modelo.LlamadaRunnable;

/**
 * Controlador para la gestion de peticiones desde la vista
 * @author Steven A. Lozano O.
 *
 */
@Controller
public class DispatcherController {
	
	private static final Logger logger = LoggerFactory.getLogger(DispatcherController.class);
	private static List<Empleado> 	listaEmpleados 	= new ArrayList<Empleado>();
	private static List<Cliente>  	listaCliente	= new ArrayList<Cliente>();
	private static List<Llamada>  	listaLlamada	= new ArrayList<Llamada>();
	private final Integer	numeroHilos		= 10;
	private ExecutorService executor;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		if ( Objects.nonNull(executor) && executor.isTerminated() )
			executor.shutdown();
		
		List<Llamada> colaLlamadas = listaLlamada.parallelStream().filter(p -> p.getEstadoLlamada().equals ( EstadoLlamadaEnum.EN_ESPERA ) ).collect(Collectors.toList());
		
		model.addAttribute ( "totalLlamadas", 		listaLlamada.size() );		
		model.addAttribute ( "llamadasFinalizadas", listaLlamada.parallelStream().filter(p -> p.getEstadoLlamada().equals ( EstadoLlamadaEnum.FINALIZADA ) ).count() );
		model.addAttribute ( "llamadasEnEspera", 	colaLlamadas.size() );
		model.addAttribute ( "colaLlamadas", 		colaLlamadas 		);
		model.addAttribute ( "listaEmpleados", 		listaEmpleados 		);
		model.addAttribute ( "listaCliente", 		listaCliente 		);
		model.addAttribute ( "listaLlamada", 		listaLlamada 		);
		
		return "home";
	}
	
	/**
	 * Se deja el agregar empleado de forma aleatoria pues el requerimiento no solicita
	 * el crud del mismo.
	 * @return
	 */
	@RequestMapping(value="/paginaPrincipal",params="agregarEmpleado", method=RequestMethod.POST)
	public String agregarEmpleado(Locale locale, Model model) {
		
		listaEmpleados.add ( new Empleado ( listaEmpleados.size()+1,
											"Empleado "+(listaEmpleados.size()+1),
											(int) (Math.random()*99999999)+1,
											TipoEmpleadoEnum.obtenerPorPrioridad ( (int) (Math.random()*3)+1 ) ) );
		logger.info("Agregado el Empleado: "+listaEmpleados.get(listaEmpleados.size()-1).getNombres());
		return "redirect:/";
	}
	
	/**
	 * Se deja el agregar llamada de forma aleatoria para el tiempo y el cliente pues el requerimiento no solicita
	 * el crud del mismo.
	 * @return
	 */
	@RequestMapping(value="/paginaPrincipal",params="agregarLlamada", method=RequestMethod.POST)
	public String agregarLlamada(Locale locale, Model model) { 
		if ( listaEmpleados.size() < 0 ) {
			logger.error("No tiene empleados para atender llamadas quedaran en estado en espera las llamadas");
		}
		//Se agrega Cliente aleatorio
		listaCliente.add(new Cliente(listaCliente.size()+1, "Cliente "+(listaEmpleados.size()+1), (int) (Math.random()*99999999)+1));
		//Se agrega la llamada en el estado correspondiente
		listaLlamada.add(new Llamada(listaLlamada.size()+1, listaCliente.get(listaCliente.size()-1).getId(), EstadoLlamadaEnum.ENTRANTE, new Date(), ( (int) (Math.random()*5)+5) ));
		
		if ( Objects.isNull(executor) || executor.isShutdown() )
			executor = Executors.newFixedThreadPool ( this.numeroHilos );
		
		List<Llamada> listaAuxiliarLlamadas = listaLlamada.parallelStream().filter(p -> p.getEstadoLlamada().equals(EstadoLlamadaEnum.ENTRANTE) ).collect(Collectors.toList());
		
		listaAuxiliarLlamadas.stream().forEach(p -> {
			listaLlamada.get ( listaLlamada.indexOf(p) ).setEstadoLlamada(EstadoLlamadaEnum.EN_ESPERA);
			Runnable llamar = new LlamadaRunnable ( p );
			executor.execute(llamar);
		});
		
		return "redirect:/";
	}
	
	/**
	 * Metodo de busqueda de empleados disponibles para la asignación de llamadas
	 * @param llamada
	 * @return
	 */
	public static Llamada dispatchCall ( Llamada llamada ) {
		if ( !listaLlamada.stream().filter(p -> p.getEstadoLlamada().equals(EstadoLlamadaEnum.EN_ESPERA)
											&& p.getFechaEntrada().before(llamada.getFechaEntrada()) ).findFirst().isPresent() ){
			List<Empleado> empleados = listaEmpleados.parallelStream().filter(p -> p.isEstaLibre()).collect(Collectors.toList());
			Empleado empleado;
			if ( empleados.size() > 0 ) {
				if ( empleados.stream().filter(p -> p.getTipoEmpleado().equals(TipoEmpleadoEnum.OPERADOR)).findFirst().isPresent() ) {
					empleado = empleados.stream().filter(p -> p.getTipoEmpleado().equals(TipoEmpleadoEnum.OPERADOR)).collect(Collectors.toList()).get(0);
				}else if ( empleados.stream().filter(p -> p.getTipoEmpleado().equals(TipoEmpleadoEnum.SUPERVISOR)).findFirst().isPresent() ) {
					empleado = empleados.stream().filter(p -> p.getTipoEmpleado().equals(TipoEmpleadoEnum.SUPERVISOR)).collect(Collectors.toList()).get(0);
				}else {
					empleado = empleados.stream().filter(p -> p.getTipoEmpleado().equals(TipoEmpleadoEnum.DIRECTOR)).collect(Collectors.toList()).get(0);
				}
				listaLlamada.get ( listaLlamada.indexOf(llamada) ).setEstadoLlamada(EstadoLlamadaEnum.EN_PROCESO);
				listaLlamada.get ( listaLlamada.indexOf(llamada) ).setIdEmpleadoAtendio(empleado.getId());
				llamada.setEstadoLlamada	 ( EstadoLlamadaEnum.EN_PROCESO );
				llamada.setIdEmpleadoAtendio ( empleado.getId() );
				listaEmpleados.get(listaEmpleados.indexOf(empleado)).setEstaLibre(false);
			}
		}
		return llamada;
	}
	
	public static void terminaLlamada ( Llamada llamada ) {
		listaLlamada.get ( listaLlamada.indexOf(llamada) ).setEstadoLlamada(EstadoLlamadaEnum.FINALIZADA);
		listaLlamada.get ( listaLlamada.indexOf(llamada) ).setFechaFinaliza(new Date());
		Empleado empleado = listaEmpleados.parallelStream().filter(p -> p.getId().equals(llamada.getIdEmpleadoAtendio())).collect(Collectors.toList()).get(0);
		listaEmpleados.get(listaEmpleados.indexOf(empleado)).setEstaLibre(true);
	}
	
	public static void cambiarEstadoListaLlamada ( Llamada llamada, EstadoLlamadaEnum estado ){
		listaLlamada.get ( listaLlamada.indexOf(llamada) ).setEstadoLlamada(estado);
    }

	public List<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	public List<Cliente> getListaCliente() {
		return listaCliente;
	}

	public List<Llamada> getListaLlamada() {
		return listaLlamada;
	}
}
