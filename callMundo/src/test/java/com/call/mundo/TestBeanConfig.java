package com.call.mundo;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.call.mundo")
public class TestBeanConfig {

}